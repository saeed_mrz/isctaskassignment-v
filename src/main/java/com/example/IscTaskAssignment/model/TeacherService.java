package com.example.IscTaskAssignment.model;

import com.example.IscTaskAssignment.model.dto.TeacherDto;

import java.util.Set;

public interface TeacherService {

    Long addTeacher(TeacherDto teacherDto);

    void assignCourseToTeacher(Long teacherId, Set<Long> courseIds);

    void deleteCourseToTeacher(Long teacherId, Set<Long> courseIds);

}

