package com.example.IscTaskAssignment.model.dto;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
@AllArgsConstructor

public class StudentDto {
    @NotNull
    private Long studentId;

    @NotNull
    private String studentName;
}
