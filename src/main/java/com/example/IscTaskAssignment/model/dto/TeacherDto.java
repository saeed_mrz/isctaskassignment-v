package com.example.IscTaskAssignment.model.dto;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor

public class TeacherDto {
    @NotNull
    private Long teacherId;

    @NotNull
    private String teacherName;
}
