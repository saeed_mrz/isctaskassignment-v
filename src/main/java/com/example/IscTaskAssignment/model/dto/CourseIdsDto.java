package com.example.IscTaskAssignment.model.dto;

import java.util.Set;

public class CourseIdsDto {

   private Set<Long> courseIds;

   public Set<Long> getCourseIds() {
      return courseIds;
   }

   public void setCourseIds(Set<Long> courseIds) {
      this.courseIds = courseIds;
   }
}
