package com.example.IscTaskAssignment.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class AvailableCoursesDto {
    private Long courseId;
    private String courseName;
    private Integer courseUnit;
    private Set<TeacherDto> teachers;
}
