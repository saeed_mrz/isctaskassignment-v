package com.example.IscTaskAssignment.model.dto;


import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor

public class CourseDto {
    @NotNull
    private Long courseId;


    @NotNull
    @NotEmpty
    private String courseName;

    @NotNull
    private Integer courseUnit;
}
