package com.example.IscTaskAssignment.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UnitSelectionDto {

    private Long courseId;
    private Long teacherId;

}
