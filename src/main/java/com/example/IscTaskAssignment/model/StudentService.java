package com.example.IscTaskAssignment.model;

import com.example.IscTaskAssignment.model.dto.StudentDto;

public interface StudentService {

    Long addStudent(StudentDto studentDto);
}
