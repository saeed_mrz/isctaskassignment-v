package com.example.IscTaskAssignment.model;

import com.example.IscTaskAssignment.model.dto.AvailableCoursesDto;
import com.example.IscTaskAssignment.model.dto.CourseDto;

import java.util.List;

public interface CourseService {

    Long addCourse(CourseDto courseDto);

    List<AvailableCoursesDto> availableCourses();
}
