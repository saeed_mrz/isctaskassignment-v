package com.example.IscTaskAssignment.config.exception;

public class BusinessException extends RuntimeException {

    private Integer errorCode;
    private String errorDesc;

    public BusinessException(Integer code, String desc) {
        super();
        this.errorCode = code;
        this.errorDesc = desc;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    @Override
    public String getMessage(){
        return this.errorDesc;
    }
}
