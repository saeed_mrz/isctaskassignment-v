package com.example.IscTaskAssignment.config.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("NullableProblems")
@RestControllerAdvice
public class RestfulExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String DEFAULT_ERROR_CODE = "1000";

    @ExceptionHandler
    protected ResponseEntity<List<ErrorDto>> handleException(Exception ex){
        ErrorDto errorDto = new ErrorDto();
        errorDto.setCode(DEFAULT_ERROR_CODE);
        errorDto.setMessage(ex.getMessage());
        return new ResponseEntity<>(Collections.singletonList(errorDto), HttpStatus.INTERNAL_SERVER_ERROR);
    }




    @ExceptionHandler(BusinessException.class)
    protected ResponseEntity<List<ErrorDto>> handleValidation(BusinessException exception) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setCode(String.valueOf(exception.getErrorCode()));
        errorDto.setMessage(String.valueOf(exception.getErrorDesc()));
        return new ResponseEntity<>(Collections.singletonList(errorDto), HttpStatus.BAD_REQUEST);
    }

}
