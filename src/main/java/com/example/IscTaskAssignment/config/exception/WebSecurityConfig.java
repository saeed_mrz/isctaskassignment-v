package com.example.IscTaskAssignment.config.exception;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.http.HttpMethod.POST;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf((csrf) -> csrf.disable());

        http.authorizeHttpRequests(authorize -> authorize

                .requestMatchers( "/admin/**").hasRole("ADMIN")
                .requestMatchers( "/teacher/**").hasRole("TEACHER")
                .requestMatchers( "/student/**").hasRole("STUDENT")
                .requestMatchers("/h2-console/**","course/availableCourses").permitAll()
                .requestMatchers("/v3/api-docs/**",
                        "/swagger-ui/**").permitAll()
                .anyRequest().authenticated()
        );
        http.headers(headers -> headers
                .frameOptions(frameOptions -> frameOptions
                        .sameOrigin()
                ));
        http.httpBasic(Customizer.withDefaults());
        return http.build();
    }

    @Bean
    public UserDetailsService users() {
        UserDetails admin = User.builder()
                .username("admin")
                .password("{noop}admin")
                .roles("ADMIN", "STUDENT", "TEACHER")
                .build();

        UserDetails student = User.builder()
                .username("student")
                .password("{noop}student")
                .roles("STUDENT")
                .build();
        UserDetails teacher = User.builder()
                .username("teacher")
                .password("{noop}teacher")
                .roles("TEACHER")
                .build();

        return new InMemoryUserDetailsManager(admin, teacher, student);
    }
}
