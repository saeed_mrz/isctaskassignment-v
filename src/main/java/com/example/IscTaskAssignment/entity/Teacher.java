package com.example.IscTaskAssignment.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "teacher")
@Getter
@Setter
@Component
public class Teacher extends BaseEntity{

    @Id
    @Column(name = "TEACHER_ID")
    private Long teacherId;

    @Column(name = "FULL_NAME")
    private String teacherName;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Teacher_Course",
            joinColumns = { @JoinColumn(name = "TEACHER_ID") },
            inverseJoinColumns = { @JoinColumn(name = "COURSE_ID") }
    )
    Set<Course> courses = new HashSet<>();

}

