package com.example.IscTaskAssignment.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity<ID extends Serializable>  {



    @Version
    @Column(name = "ROW_VERSION")
    protected Integer version;

    @Column(name = "CREATE_DATE")
    protected LocalDateTime createDate;

    @Column(name = "LAST_EDIT_DATE")
    protected LocalDateTime lastEditDate;

    @PrePersist
    protected void onCreate() {
        lastEditDate = createDate = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        lastEditDate = LocalDateTime.now();
    }


}
