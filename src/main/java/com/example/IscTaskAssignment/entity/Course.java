package com.example.IscTaskAssignment.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "course")
@Getter
@Setter
@Component
@AllArgsConstructor
@NoArgsConstructor
public class Course extends BaseEntity {

    @Id
    @Column(name = "COURSE_ID")
    private Long courseId;

    @Column(name = "COURSE_NAME")
    private String courseName;

    @Column(name = "COURSE_UNIT")
    private Integer courseUnit;


    @ManyToMany(mappedBy = "courses")
    private Set<Teacher> teachers = new HashSet<>();
}
