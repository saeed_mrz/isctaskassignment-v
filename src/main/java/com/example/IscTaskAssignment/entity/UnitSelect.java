package com.example.IscTaskAssignment.entity;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "unit")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UnitSelect extends BaseEntity<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UNIT_SELECT_ID")
    private Long unitSelectId;

    @Column(name = "STUDENT_ID")
    private Long studentId;

    @Column(name = "TEACHER_ID")
    private Long teacherId;

    @Column(name = "COURSE_ID")
    private Long courseId;






}
