package com.example.IscTaskAssignment.entity;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "student")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Student extends BaseEntity<Long>{


    @Id
    @Column(name = "STUDENT_ID")
    private Long studentId;

    private String studentName;

}
