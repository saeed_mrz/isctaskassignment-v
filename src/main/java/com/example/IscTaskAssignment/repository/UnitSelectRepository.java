package com.example.IscTaskAssignment.repository;

import com.example.IscTaskAssignment.entity.UnitSelect;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UnitSelectRepository extends CrudRepository<UnitSelect,Long> {


    Optional<List<UnitSelect>> findByStudentId(Long studentId);
}
