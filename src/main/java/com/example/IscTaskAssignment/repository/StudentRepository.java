package com.example.IscTaskAssignment.repository;

import com.example.IscTaskAssignment.entity.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
}
