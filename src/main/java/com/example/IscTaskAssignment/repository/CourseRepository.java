package com.example.IscTaskAssignment.repository;

import com.example.IscTaskAssignment.entity.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long> {

    Set<Course> findAllByCourseIdIn(Set<Long> courseId);
}
