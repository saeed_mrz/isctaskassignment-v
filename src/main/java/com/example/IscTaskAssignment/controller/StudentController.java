package com.example.IscTaskAssignment.controller;

import com.example.IscTaskAssignment.model.StudentService;
import com.example.IscTaskAssignment.model.dto.StudentDto;
import com.example.IscTaskAssignment.model.dto.UnitSelectionDto;
import com.example.IscTaskAssignment.service.StudentServiceImpl;
import com.example.IscTaskAssignment.service.UnitSelectionService;
import io.swagger.v3.oas.annotations.tags.Tag;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@Tag(name = "Student")
@RestController
@RequestMapping("student")
public class StudentController {

    private final StudentService studentService;
    private final UnitSelectionService unitSelectionService;


    public StudentController(StudentService studentService, UnitSelectionService unitSelectionService) {
        this.studentService = studentService;
        this.unitSelectionService = unitSelectionService;
    }




    @PostMapping("/{studentId}/unitSelection")
    public ResponseEntity<Long> addUnitSelection(@RequestBody @Valid UnitSelectionDto unitSelectionDto, @PathVariable Long studentId){

        Long unitSelectionId = unitSelectionService.addUnitSelection(unitSelectionDto,studentId);

        return new ResponseEntity<>(unitSelectionId, HttpStatus.CREATED);

    }

}
