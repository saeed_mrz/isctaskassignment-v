package com.example.IscTaskAssignment.controller;


import com.example.IscTaskAssignment.model.TeacherService;
import com.example.IscTaskAssignment.model.dto.CourseIdsDto;
import com.example.IscTaskAssignment.model.dto.TeacherDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Teacher")
@RestController
@RequestMapping("teacher")
public class TeacherController {

    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PutMapping("/{teacherId}/courses/")
    public ResponseEntity assignCourseToTeacher(@RequestBody CourseIdsDto courseIdsDto,@PathVariable Long teacherId){

        teacherService.assignCourseToTeacher(teacherId,courseIdsDto.getCourseIds());

        return new ResponseEntity<>(HttpStatus.CREATED);

    }
    @DeleteMapping("/{teacherId}/courses/")
    public ResponseEntity deleteCourseFromTeacher(@RequestBody CourseIdsDto courseIdsDto,@PathVariable Long teacherId){

        teacherService.deleteCourseToTeacher(teacherId,courseIdsDto.getCourseIds());

        return new ResponseEntity<>(HttpStatus.OK);

    }
}
