package com.example.IscTaskAssignment.controller;

import com.example.IscTaskAssignment.model.CourseService;
import com.example.IscTaskAssignment.model.StudentService;
import com.example.IscTaskAssignment.model.TeacherService;
import com.example.IscTaskAssignment.model.dto.CourseDto;
import com.example.IscTaskAssignment.model.dto.StudentDto;
import com.example.IscTaskAssignment.model.dto.TeacherDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Admin")
@RestController
@RequestMapping("admin")
public class AdminController {

    private final StudentService studentService;
    private final TeacherService teacherService;
    private final CourseService courseService;


    public AdminController(StudentService studentService, TeacherService teacherService, CourseService courseService) {
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.courseService = courseService;
    }

    @PostMapping("/student")
    public ResponseEntity<Long> addStudent(@RequestBody @Valid StudentDto studentDto){

        Long studentId = studentService.addStudent(studentDto);

        return new ResponseEntity<>(studentId, HttpStatus.CREATED);

    }
    @PostMapping("/teacher")
    public ResponseEntity<Long> addTeacher(@RequestBody @Valid TeacherDto teacherDto){

        Long teacherId = teacherService.addTeacher(teacherDto);

        return new ResponseEntity<>(teacherId, HttpStatus.CREATED);

    }

    @PostMapping("/course")
    public ResponseEntity<Long> addCourse(@RequestBody @Valid CourseDto courseDto){

        Long courseId = courseService.addCourse(courseDto);

        return new ResponseEntity<>(courseId, HttpStatus.CREATED);

    }
}
