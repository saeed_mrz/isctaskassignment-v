package com.example.IscTaskAssignment.controller;

import com.example.IscTaskAssignment.model.CourseService;
import com.example.IscTaskAssignment.model.dto.AvailableCoursesDto;
import com.example.IscTaskAssignment.model.dto.CourseDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Course")
@RestController
@RequestMapping("course")
public class CourseController {

    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/availableCourses")
    public ResponseEntity<List<AvailableCoursesDto>> availableCourses(){

       List<AvailableCoursesDto> availableCoursesDtos = courseService.availableCourses();

        return new ResponseEntity<>(availableCoursesDtos, HttpStatus.CREATED);

    }
}
