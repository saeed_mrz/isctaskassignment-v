package com.example.IscTaskAssignment.service;


import com.example.IscTaskAssignment.config.exception.BusinessException;
import com.example.IscTaskAssignment.entity.UnitSelect;
import com.example.IscTaskAssignment.model.dto.UnitSelectionDto;
import com.example.IscTaskAssignment.repository.UnitSelectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UnitSelectionService {

    private final UnitSelectRepository unitSelectRepository;
    private Logger log = LoggerFactory.getLogger(UnitSelectionService.class);

    public UnitSelectionService(UnitSelectRepository unitSelectRepository) {
        this.unitSelectRepository = unitSelectRepository;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Long addUnitSelection(UnitSelectionDto unitSelectionDto, Long studentId) {
        Optional<List<UnitSelect>> unitSelectOptional = unitSelectRepository.findByStudentId(studentId);

        if (unitSelectOptional.isPresent() && unitSelectOptional.get().size() > 0) {
            List<UnitSelect> unitSelectList = unitSelectOptional.get();
            checkDuplicateRecordsUnitSelect(unitSelectList,unitSelectionDto);
        }

        UnitSelect newUnitSelect = new UnitSelect();
        newUnitSelect.setStudentId(studentId);
        newUnitSelect.setCourseId(unitSelectionDto.getCourseId());
        newUnitSelect.setTeacherId(unitSelectionDto.getTeacherId());
        UnitSelect savedUnitSelect = unitSelectRepository.save(newUnitSelect);
        return savedUnitSelect.getUnitSelectId();
    }
    private void checkDuplicateRecordsUnitSelect(List<UnitSelect> unitSelectList, UnitSelectionDto unitSelectionDto){
        if(unitSelectList.stream().anyMatch(unitSelect ->
                unitSelect.getCourseId().equals(unitSelectionDto.getCourseId()) &&
                        unitSelect.getTeacherId().equals(unitSelectionDto.getTeacherId()))) {
            log.error("UnitSelect is duplicate");
            throw new BusinessException(900, "UnitSelect is duplicate");
        }
    }

}
