package com.example.IscTaskAssignment.service;


import com.example.IscTaskAssignment.config.exception.BusinessException;
import com.example.IscTaskAssignment.model.TeacherService;
import com.example.IscTaskAssignment.model.dto.TeacherDto;
import com.example.IscTaskAssignment.entity.Course;
import com.example.IscTaskAssignment.entity.Teacher;
import com.example.IscTaskAssignment.repository.CourseRepository;
import com.example.IscTaskAssignment.repository.TeacherRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class TeacherServiceImpl implements TeacherService {
    private final TeacherRepository teacherRepository;
    private final CourseRepository courseRepository;

    private Logger log = LoggerFactory.getLogger(TeacherServiceImpl.class);

    public TeacherServiceImpl(TeacherRepository teacherRepository, CourseRepository courseRepository) {
        this.teacherRepository = teacherRepository;
        this.courseRepository = courseRepository;
    }

    public Long addTeacher(TeacherDto teacherDto) {

        Optional<Teacher> optionalTeacher = teacherRepository.findById(teacherDto.getTeacherId());
        if (optionalTeacher.isPresent()) {
            log.error("Teacher is duplicate --> teacherId :" + teacherDto.getTeacherId());

            throw new BusinessException(800, "Teacher is duplicate --> teacherId : " + teacherDto.getTeacherId());
        }

        Teacher newTeacher = new Teacher();
        newTeacher.setTeacherId(teacherDto.getTeacherId());
        newTeacher.setTeacherName(teacherDto.getTeacherName());
        Teacher savedTeacher = teacherRepository.save(newTeacher);
        return savedTeacher.getTeacherId();
    }

    public void assignCourseToTeacher(Long teacherId, Set<Long> courseIds) {

        Optional<Teacher> optionalTeacher = teacherRepository.findById(teacherId);
        if (optionalTeacher.isPresent()) {

            Teacher teacher = optionalTeacher.get();
            teacher.setCourses(makeCourses(courseIds));
            teacherRepository.save(teacher);
            return;
        }
        throw new BusinessException(801, "Teacher not found --> teacherId : " + teacherId);
    }

    @Override
    public void deleteCourseToTeacher(Long teacherId, Set<Long> courseIds) {
        Optional<Teacher> optionalTeacher = teacherRepository.findById(teacherId);
        if (optionalTeacher.isPresent()) {

            Teacher teacher = optionalTeacher.get();
            teacher.setCourses(removeCourses(optionalTeacher.get().getCourses() ,courseIds));
            teacherRepository.save(teacher);
            return;
        }
        throw new BusinessException(801, "Teacher not found --> teacherId : " + teacherId);
    }

    private Set<Course> makeCourses(Set<Long> courseIds) {
        Set<Course> courses = courseRepository.findAllByCourseIdIn(courseIds);
        return courses;
    }

    private Set<Course> removeCourses(Set<Course> courseSetToRemove , Set<Long> courseIds) {
        Set<Course> courses = courseRepository.findAllByCourseIdIn(courseIds);
        courseSetToRemove.removeAll(courses);
        return courseSetToRemove;
    }
}
