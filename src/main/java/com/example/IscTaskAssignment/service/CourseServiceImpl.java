package com.example.IscTaskAssignment.service;

import com.example.IscTaskAssignment.config.exception.BusinessException;
import com.example.IscTaskAssignment.entity.Course;
import com.example.IscTaskAssignment.entity.Teacher;
import com.example.IscTaskAssignment.model.CourseService;
import com.example.IscTaskAssignment.model.dto.AvailableCoursesDto;
import com.example.IscTaskAssignment.model.dto.CourseDto;
import com.example.IscTaskAssignment.model.dto.TeacherDto;
import com.example.IscTaskAssignment.repository.CourseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private Logger log = LoggerFactory.getLogger(CourseServiceImpl.class);


    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }


    public Long addCourse(CourseDto courseDto) {

        Optional<Course> optionalCourse = courseRepository.findById(courseDto.getCourseId());
        if (optionalCourse.isPresent()) {
            log.error("Course is duplicate --> CourseId" + courseDto.getCourseId());
            throw new BusinessException(300, "Course is duplicate --> CourseId : " + courseDto.getCourseId());
        }

        Course newCourse = new Course();
        newCourse.setCourseId(courseDto.getCourseId());
        newCourse.setCourseName(courseDto.getCourseName());
        newCourse.setCourseUnit(courseDto.getCourseUnit());
        Course savedCourse = courseRepository.save(newCourse);

        log.info("CourseId success generated.");

        return savedCourse.getCourseId();
    }

    public List<AvailableCoursesDto> availableCourses() {
        Iterable<Course> courseList = courseRepository.findAll();
        return makeAvailableCoursesDtos(courseList);
    }

    private List<AvailableCoursesDto> makeAvailableCoursesDtos(Iterable<Course> courseList) {
        return StreamSupport.stream(courseList.spliterator(),false).map(course -> {
            return new AvailableCoursesDto(course.getCourseId(), course.getCourseName(), course.getCourseUnit(), makeTeacherDto(course.getTeachers()));
        }).collect(Collectors.toList());
    }

    private Set<TeacherDto> makeTeacherDto(Set<Teacher> teachers) {

        return teachers.stream().map(teacher -> {
            return new TeacherDto(teacher.getTeacherId(), teacher.getTeacherName());
        }).collect(Collectors.toSet());
    }

}
