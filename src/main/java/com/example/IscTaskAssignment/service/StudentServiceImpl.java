package com.example.IscTaskAssignment.service;

import com.example.IscTaskAssignment.config.exception.BusinessException;
import com.example.IscTaskAssignment.model.dto.StudentDto;
import com.example.IscTaskAssignment.entity.Student;
import com.example.IscTaskAssignment.model.StudentService;
import com.example.IscTaskAssignment.repository.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private Logger log = LoggerFactory.getLogger(StudentServiceImpl.class);

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Long addStudent(StudentDto studentDto) {

        Optional<Student> studentOptional = studentRepository.findById(studentDto.getStudentId());
        if (studentOptional.isPresent()) {
            log.error("Student is duplicate --> studentId : " + studentDto.getStudentId());

            throw new BusinessException(700, "Student is duplicate --> studentId : " + studentDto.getStudentId());
        }
        Student newStudent = new Student();
        newStudent.setStudentId(studentDto.getStudentId());
        newStudent.setStudentName(studentDto.getStudentName());
        Student savedStudent = studentRepository.save(newStudent);
        return savedStudent.getStudentId();

    }
}
