package com.example.IscTaskAssignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IscTaskAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(IscTaskAssignmentApplication.class, args);
	}

}
