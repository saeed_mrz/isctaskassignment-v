package com.example.IscTaskAssignment.repository;

import com.example.IscTaskAssignment.entity.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

@DataJpaTest
public class StudentRepositoryTest {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentRepositoryTest(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @BeforeEach
    void saveStudents() {
        var students = List.of(
                new Student(1L, "stu1"),
                new Student(2L, "stu2")

        );

        studentRepository.saveAll(students);
    }

    @Test
    void saveStudentWithStudentId() {
        studentRepository.save(
                new Student(3L, "stu3")
        );
        Assertions.assertEquals(3, studentRepository.findById(3L).get().getStudentId());
    }
}
