package com.example.IscTaskAssignment.repository;

import com.example.IscTaskAssignment.entity.Course;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;
import java.util.Set;
@DataJpaTest
public class CourseRepositoryTest {

    private final CourseRepository courseRepository;

    @Autowired
    public CourseRepositoryTest(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @BeforeEach
    void saveCourses() {
        var courses = List.of(
                new Course(111L, "ریاضی", 3, null),
                new Course(222L, "فیزیک", 2, null),
                new Course(333L, "ساختمان داده", 3, null),
                new Course(444L, "برنامه نویسی پیشرفته", 3, null)
        );
        courseRepository.saveAll(courses);
    }


    @Test
    void findCoursesByIds() {
        Optional<Course> fizik = courseRepository.findById(222L);
        Optional<Course> sakhteman = courseRepository.findById(333L);
        Set<Long> Ids = Set.of(222L, 333L);
        Assertions.assertEquals(Set.of(fizik.get(), sakhteman.get()), courseRepository.findAllByCourseIdIn(Ids));
    }
}
