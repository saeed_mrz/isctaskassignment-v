package com.example.IscTaskAssignment.service;

import com.example.IscTaskAssignment.config.UnitSelectRepositoryStub;
import com.example.IscTaskAssignment.config.exception.BusinessException;
import com.example.IscTaskAssignment.entity.UnitSelect;
import com.example.IscTaskAssignment.model.dto.UnitSelectionDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class UnitSelectionServiceTest {

    private UnitSelectRepositoryStub unitSelectRepository = new UnitSelectRepositoryStub();

    private UnitSelectionService unitSelectionService = new UnitSelectionService(unitSelectRepository);

    @BeforeEach
    void setUp() {
        UnitSelect unitSelectStudent1 = UnitSelect.builder()
                .studentId(1L)
                .courseId(100L)
                .teacherId(200L)
                .build();
        unitSelectRepository.save(unitSelectStudent1);
    }

    @Test
    void addDuplicatedUnitSelectMustGeneratedBusinessExceptionCode900() {
        UnitSelectionDto unitSelectStudent1 = new UnitSelectionDto(100L, 200L);
        BusinessException businessException = assertThrows(BusinessException.class,

                () -> {
                    unitSelectionService.addUnitSelection(unitSelectStudent1,1L);
                }

        );
        Assertions.assertTrue(businessException.getErrorCode().equals(900));

    }
}