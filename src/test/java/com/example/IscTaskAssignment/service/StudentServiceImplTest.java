package com.example.IscTaskAssignment.service;

import com.example.IscTaskAssignment.config.StudentRepositoryStub;
import com.example.IscTaskAssignment.config.exception.BusinessException;
import com.example.IscTaskAssignment.model.dto.StudentDto;
import com.example.IscTaskAssignment.entity.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
class StudentServiceImplTest {

    private StudentRepositoryStub studentRepository = new StudentRepositoryStub();
    private StudentServiceImpl studentService = new StudentServiceImpl(studentRepository);

    @BeforeEach
    void setUp() {
        Student ali = Student.builder()
                .studentId(1L)
                .studentName("Ali")
                .build();
        studentRepository.save(ali);
    }

    @Test
    void addDuplicatedStudentMustGeneratedBusinessException() {
        StudentDto ali = new StudentDto(1L, "Ali");

        BusinessException businessException = assertThrows(BusinessException.class,

                () -> {
                    studentService.addStudent(ali);
                }

        );
        Assertions.assertTrue(businessException.getErrorCode().equals(700));

    }
}