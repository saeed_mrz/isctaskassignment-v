package com.example.IscTaskAssignment.config;

import com.example.IscTaskAssignment.entity.Student;
import com.example.IscTaskAssignment.repository.StudentRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class StudentRepositoryStub implements StudentRepository {
    private Map<Long, Student> studentMap;


    public StudentRepositoryStub() {
        this.studentMap = new HashMap<>();
    }
    @Override
    public Optional<Student> findById(Long id) {
        if (studentMap.containsKey(id)) {
            return Optional.of(studentMap.get(id));
        }else {
            return Optional.empty();
        }
    }

    @Override
    public <S extends Student> S save(S entity) {
        studentMap.put(entity.getStudentId(), entity);
        return entity;
    }

    @Override
    public <S extends Student> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }


    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<Student> findAll() {
        return null;
    }

    @Override
    public Iterable<Student> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Student entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends Student> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
