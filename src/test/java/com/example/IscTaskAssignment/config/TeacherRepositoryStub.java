package com.example.IscTaskAssignment.config;

import com.example.IscTaskAssignment.entity.Teacher;
import com.example.IscTaskAssignment.repository.TeacherRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class TeacherRepositoryStub implements TeacherRepository {
    private Map<Long, Teacher> teacherMap;


    public TeacherRepositoryStub() {
        this.teacherMap = new HashMap<>();
    }
    @Override
    public Optional<Teacher> findById(Long id) {
        if (teacherMap.containsKey(id)) {
            return Optional.of(teacherMap.get(id));
        }else {
            return Optional.empty();
        }
    }

    @Override
    public <S extends Teacher> S save(S entity) {
        teacherMap.put(entity.getTeacherId(), entity);
        return entity;
    }

    @Override
    public <S extends Teacher> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }


    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<Teacher> findAll() {
        return null;
    }

    @Override
    public Iterable<Teacher> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Teacher entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends Teacher> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
