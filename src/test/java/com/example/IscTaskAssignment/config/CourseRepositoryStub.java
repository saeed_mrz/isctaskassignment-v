package com.example.IscTaskAssignment.config;

import com.example.IscTaskAssignment.entity.Course;
import com.example.IscTaskAssignment.repository.CourseRepository;

import java.util.*;

public class CourseRepositoryStub implements CourseRepository {
    private Map<Long, Course> courseMap;


    public CourseRepositoryStub() {
        this.courseMap = new HashMap<>();
    }
    @Override
    public Optional<Course> findById(Long id) {
        if (courseMap.containsKey(id)) {
            return Optional.of(courseMap.get(id));
        }else {
            return Optional.empty();
        }
    }

    @Override
    public <S extends Course> S save(S entity) {
        courseMap.put(entity.getCourseId(), entity);
        return entity;
    }

    @Override
    public Set<Course> findAllByCourseIdIn(Set<Long> inputCourseIds) {
        Set<Course> courses = new HashSet<>();
        for (Map.Entry<Long, Course> courseMap : courseMap.entrySet()) {
            if(inputCourseIds.contains(courseMap.getValue().getCourseId()))
            courses.add(courseMap.getValue());
        }
        return courses;
    }

    @Override
    public <S extends Course> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }


    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<Course> findAll() {
        return null;
    }

    @Override
    public Iterable<Course> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Course entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends Course> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
