package com.example.IscTaskAssignment.config;

import com.example.IscTaskAssignment.entity.UnitSelect;
import com.example.IscTaskAssignment.repository.UnitSelectRepository;

import java.util.*;

public class UnitSelectRepositoryStub implements UnitSelectRepository {
    private Map<Long, UnitSelect> unitSelectMap;


    public UnitSelectRepositoryStub() {
        this.unitSelectMap = new HashMap<>();
    }
    @Override
    public Optional<UnitSelect> findById(Long id) {
        if (unitSelectMap.containsKey(id)) {
            return Optional.of(unitSelectMap.get(id));
        }else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<List<UnitSelect>> findByStudentId(Long studentId) {
     if(!unitSelectMap.isEmpty()){
        List<UnitSelect> unitSelectList = new ArrayList<>();
         for (Map.Entry<Long, UnitSelect> unitSelectEntry : unitSelectMap.entrySet()) {
             if(studentId.equals(unitSelectEntry.getValue().getStudentId()))
                 unitSelectList.add(unitSelectEntry.getValue());
         }
        return Optional.of(unitSelectList);
    }else

    {
        return Optional.empty();
    }
    }

    @Override
    public <S extends UnitSelect> S save(S entity) {
        unitSelectMap.put(entity.getUnitSelectId(), entity);
        return entity;
    }

    @Override
    public <S extends UnitSelect> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }


    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<UnitSelect> findAll() {
        return null;
    }

    @Override
    public Iterable<UnitSelect> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(UnitSelect entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends UnitSelect> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
